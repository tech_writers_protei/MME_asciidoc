---
title: "HTTP EDR"
description: "Журнал http_cdr"
type: docs
weight: 20
---
= HTTP EDR
include::../../ascii_parameters.adoc[]

.Описание используемых полей
[options="header",cols="1,3,8,2"]

|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
`YYYY-MM-DD HH:MM:SS`.
| datetime

| 2
| <<_event_http,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 4
| IMSI
| Номер IMSI абонента.
| string

| 5
| MSISDN
| Номер MSISDN абонента.
| string

| 6
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 7
| PLMN
| Идентификатор PLMN.
| string

| 8
| CellID
| Идентификатор соты.
| int/hex

| 9
| IMEI
| Номер IMEI устройства.
| string

| 10
| Duration
| Длительность процедуры.
| int +
мс

| 11
| ErrorCode
| Внутренний <<common/logging/edr/error_code.adoc#_error_codes,код MME>> результата.
| object
|===

[[_event_http]]
== События и процедуры, Event

* <<_deact_bearer_http,Deact bearer>>;
* <<_detach_http,Detach>>;
* <<_disconnect_enodeb_http,Disconnect eNodeB>>;
* <<_get_profile_http,Get Profile>>;
* <<_get_db_status_http,Get DB Status>>;
* <<_get_gtp_peers_http,Get GTP Peers>>;
* <<_get_s1_peers_http,Get S1 Peers>>;
* <<_get_sgs_peers_http,Get SGs Peers>>;
* <<_clear_dns_cache_http,Clear DNS Cache>>;
* <<_ue_enable_trace_http,UE enable trace>>;
* <<_ue_disable_trace_http,UE disable trace>>;
* <<_get_metrics_http,Get Metrics>>;
* <<_reset_metrics_http,Reset Metrics>>.

[[_deact_bearer_http]]
.Deact Bearer
[source,log]
----
DateTime,Deact bearer,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Не удалось получить идентификатор bearer-службы.

| 2
| Неизвестный идентификатор bearer-службы.

| 3
| Ошибка процедуры Paging.

| 4
| Ошибка деактивации bearer-службы.
|===

[[_detach_http]]
.Detach
[source,log]
----
DateTime,Detach,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Ошибка процедуры Paging.

| 2
| Ошибка процедуры Detach.
|===

[[_disconnect_enodeb_http]]
.Disconnect eNodeB
[source,log]
----
DateTime,Disconnect eNodeB,IP,Duration,ErrorCode
----

[[_get_profile_http]]
.Get Profile
[source,log]
----
DateTime,GetProfile,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[[_get_db_status_http]]
.Get DB Status
[source,log]
----
DateTime,Get DB Status,Duration,ErrorCode
----

[[_get_gtp_peers_http]]
.Get GTP Peers
[source,log]
----
DateTime,Get GTP Peers,Duration,ErrorCode
----

[[_get_s1_peers_http]]
.Get S1 Peers
[source,log]
----
DateTime,Get S1 Peers,Duration,ErrorCode
----

[[_get_sgs_peers_http]]
.Get SGs Peers
[source,log]
----
DateTime,Get SGS Peers,Duration,ErrorCode
----

[[_clear_dns_cache_http]]
.Clear DNS Cache
[source,log]
----
DateTime,Clear DNS Cache,Duration,ErrorCode
----

[[_ue_enable_trace_http]]
.UE enable trace
[source,log]
----
DateTime,UE enable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Абонент уже находится в списке отслеживания.

| 2
| Не указан IP-адрес узла TCE.

| 3
| Ошибка процедуры Paging.

| 4
| Не найдены свободные идентификаторы Trace ID.
|===

[[_ue_disable_trace_http]]
.UE disable trace
[source,log]
----
DateTime,UE disable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Ошибка процедуры Paging.

| 2
| Идентификатор E-UTRAN Trace ID не найден для абонента.
|===

[[_get_metrics_http]]
.Get Metrics
[source,log]
----
DateTime,Get Metrics,Duration,ErrorCode
----

[[_reset_metrics_http]]
.Reset Metrics
[source,log]
----
DateTime,Reset Metrics,Duration,ErrorCode
----
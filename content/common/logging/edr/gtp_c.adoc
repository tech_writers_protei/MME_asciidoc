---
title: "GTP-C EDR"
description: "Журнал gtp_c_cdr"
type: docs
weight: 20
---
= GTP-C EDR
include::../../ascii_parameters.adoc[]

.Описание используемых полей
[options="header",cols="1,3,8,2"]

|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
`YYYY-MM-DD HH:MM:SS`.
| datetime

| 2
| <<_event_gtp_c,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| IMSI
| Номер IMSI абонента.
| string

| 4
| MSISDN
| Номер MSISDN абонента.
| string

| 5
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 6
| PLMN
| Идентификатор {PLMN}.
| string

| 7
| CellID
| Идентификатор соты.
| int/hex

| 8
| IMEI
| Номер IMEI устройства.
| string

| 9
| APN
| Имя точки доступа.
| string

| 10
| SGW IP
| IP-адрес узла SGW.
| ip

| 11
| SGW TEID
| Идентификатор конечной точки туннеля на узле SGW.
| string

| 12
| Self TEID
| Идентификатор конечной точки туннеля на узле MME.
| string

| 13
| Cause
| Код причины разрыва соединения. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf[3GPP TS 29.274].
| int

| 14
| Duration
| Длительность процедуры.
| int +
мс

| 15
| ErrorCode
| Внутренний <<common/logging/edr/error_code.adoc#_error_codes,код MME>> результата.
| object
|===

[[_event_gtp_c]]
== События и процедуры, Event

* <<_create_indirect_data_forwarding_tunnel,Create Indirect Data Forwarding Tunnel>>;
* <<_create_session_response,Create Session Response>>;
* <<_connection_lost,Connection Lost>>;
* <<_context,Context>>;
* <<_detach_notification,Detach Notification>>;
* <<_delete_bearer_request,Delete Bearer Request>>;
* <<_delete_indirect_data_forwarding_tunnel,Delete Indirect Data Forwarding Tunnel>>;
* <<_delete_session_response,Delete Session Response>>;
* <<_downlink_data_notification,Downlink Data Notification>>;
* <<_forward_relocation_request_mme,Forward Relocation Request MME>>;
* <<_modify_bearer_response,Modify Bearer Response>>;
* <<_pgw_restart_notification,PGW Restart Notification>>;
* <<_release_access_bearers_response,Release Access Bearers Response>>;
* <<_remote_restart,Remote Restart>>;
* <<_sgsn_context,SGSN Context>>;
* <<_identification_request,Identification Request>>;
* <<_update_bearer_request,Update Bearer Request>>;
* <<_ue_activity_notification,UE Activity Notification>>;
* <<_suspend,Suspend>>;
* <<_resume,Resume>>.

[[_create_indirect_data_forwarding_tunnel]]
.Create Indirect Data Forwarding Tunnel
[source,log]
----
DateTime,Create Indirect Data Forwarding Tunnel,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_create_session_response]]
.Create Session Response
[source,log]
----
DateTime,Create Session,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_connection_lost]]
.Connection Lost
[source,log]
----
DateTime,Connection Lost,IP
----

[[_context]]
.Context
[source,log]
----
DateTime,Context,IMSI,MSISDN,GUTI,PLMN,IMEI,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Не удалось декодировать сообщение GTP: TAU-Request.

| 2
| Сообщение TAU-Request не прошло проверку целостности.

| 3
| Получен отказ в сообщении GTP: Context-Acknowledgement.
|===

[[_detach_notification]]
.Detach Notification
[source,log]
----
DateTime,Detach Notification,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_delete_bearer_request]]
.Delete Bearer Request
[source,log]
----
DateTime,Delete Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Нет данных о PDN-соединениях абонента.

| 2
| Ошибка сброса контекста.

| 3
| Устройство абонента находится в режиме {PSM}.

| 4
| Флаг {PPF} уже сброшен.

| 5
| Абонент недоступен для PS-сетей.

| 6
| Получено сообщение GTP: Failure Indication от узла SGW.

| 7
| Ошибка процедуры Paging.

| 8
| Получен отказ деактивации от базовой станции.

| 9
| Указанная bearer-служба не найдена.
|===

[[_delete_indirect_data_forwarding_tunnel]]
.Delete Indirect Data Forwarding Tunnel
[source,log]
----
DateTime,Delete Indirect Data Forwarding Tunnel,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_delete_session_response]]
.Delete Session Response
[source,log]
----
DateTime,Delete Session,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Для данной сессии не указана bearer-служба по умолчанию.
|===

[[_downlink_data_notification]]
.Downlink Data Notification
[source,log]
----
DateTime,Downlink Data,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| {GUTI} абонента неизвестен.

| 2
| Нет данных о PDN-соединениях абонента.

| 3
| Ошибка процедуры Paging.

| 4
| Ошибка процедуры Service Request.

| 5
| Отказ SGW в модификации bearer-службы.

| 6
| Устройство абонента находится в состоянии {PSM}.

| 7
| Флаг {PPF} уже сброшен.

| 8
| Абонент недоступен для PS-сетей.

| 9
| Абонент находится в состоянии Suspension.
|===

[[_forward_relocation_request_mme]]
.Forward Relocation Request MME
[source,log]
----
DateTime,FwdRelocationMME,IMSI,MSISDN,GUTI,Target_PLMN,Target_TAC,Target_Cell_Id,IMEI,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Получен отказ в хэндовере.

| 2
| Узел SGW назначения не принял запрос на создание сессии.

| 3
| Нет данных о eNodeB назначения.

| 4
| Получено сообщение S1AP: HANDOVER FAILURE.

| 5
| eNodeB не приняла ни одной bearer-службы по умолчанию.

| 6
| Не получено сообщение S1AP: HANDOVER NOTIFY.

| 7
| Не найдено сообщение GTP-C:Forward Relocation Complete Acknowledge.

| 8
| Ошибка процедуры GTP-C: Modify Bearer.

| 9
| Не удалось извлечь идентификаторы IMSI, {IMEISV}.

| 10
| В сообщении Forward Relocation Request не указан {NHI}.

| 11
| Попытка хэндовера из области {TAC} сети {NB-IoT}.
|===

[[_modify_bearer_response]]
.Modify Bearer Response
[source,log]
----
DateTime,Modify Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_pgw_restart_notification]]
.PGW Restart Notification
[source,log]
----
DateTime,PGW Restart Notification,SGW IP,PGW IP
----

[[_release_access_bearers_response]]
.Release Access Bearers Response
[source,log]
----
DateTime,Release Access Bearers,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_remote_restart]]
.Remote Restart
[source,log]
----
DateTime,Remote Restart,IP,Counter
----

[[_sgsn_context]]
.SGSN Context
[source,log]
----
DateTime,SGSN Context,IMSI,MSISDN,GUTI,PLMN,IMEI,Cause,Duration,ErrorCode
----

[[_identification_request]]
.Identification Request
[source,log]
----
DateTime,Identification,IMSI,MSISDN,GUTI,Target_PLMN,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Не удалось декодировать сообщение S1 Attach Request.

| 2
| Сообщение S1 Attach Request не прошло проверку целостности.
|===

[[_update_bearer_request]]
.Update Bearer Request
[source,log]
----
DateTime,Update Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

Локальные коды ошибок:
[options="header",cols="1,9"]
|===
| N | Описание

| 1
| Для указанного абонента не разрешены PDN Connectivity.

| 2
| В запросе SGW не указаны bearer-службы.

| 3
| Вместо ожидаемого сообщения GTP-C: Update Bearer Request получено другое.

| 4
| Запрошенные bearer-службы не найдены на узле MME.

| 5
| Несовпадение идентификаторов {`PTI`}.

| 6
| Устройство абонента находится в состоянии {PSM}.

| 7
| Во время процесса Paging не получен ответ от абонента.

| 8
| Флаг {PPF} уже сброшен.

| 9
| Абонент недоступен для PS-сетей.
|===

[[_ue_activity_notification]]
.UE Activity Notification
[source,log]
----
DateTime,UE Activity Notification,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_suspend]]
.Suspend
[source,log]
----
DateTime,Suspend,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----

[[_resume]]
.Resume
[source,log]
----
DateTime,Resume,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
----
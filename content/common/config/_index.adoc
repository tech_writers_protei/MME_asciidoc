---
title: "Конфигурирование"
description: "Описание конфигурационных файлов"
type: docs
weight: 40
---
= Конфигурирование
include::../../ascii_parameters.adoc[]

Настройка узла MME осуществляется в файлах конфигурации, расположенных в директории `/usr/protei/Protei_MME/config`.

Параметры конфигурации задаются в следующих файлах:

[horizontal]
<<common/config/ap.adoc#_ap_cfg,ap.cfg>>:: конфигурация настроек подсистемы аварийной индикации;
<<common/config/apn_rules.adoc#_apn_rules_cfg,apn_rules.cfg>>:: конфигурация правил, связывающих APN NI и адреса PGW и SGW;
<<common/config/code_mapping_rules.adoc#_code_mapping_rules_cfg,code_mapping_rules.cfg>>:: конфигурация правил, связывающих Diameter: Result-Code и EMM Cause;
<<common/config/diam_dest.adoc#_diam_dest_cfg,diam_dest.cfg>>:: конфигурация правил, связывающих IMSI и параметры Destination-Host и Destination-Realm;
<<common/config/diameter.adoc#_diameter_cfg,diameter.cfg>>:: конфигурация настроек подключений по протоколу Diameter;
<<common/config/dns.adoc#_dns_cfg,dns.cfg>>:: конфигурация настроек DNS;
<<common/config/emergency_numbers.adoc#_emergency_numbers_cfg,emergency_numbers.cfg>>:: конфигурация связей между номерами экстренных служб и их типами;
<<common/config/emergency_pdn.adoc#_emergency_pdn_cfg,emergency_pdn.cfg>>:: конфигурация настроек PDN-подключений для служб передачи данных, применяемых в случае процедуры Emergency Attach без запроса профиля у узла HSS;
<<common/config/forbidden_imei_rules.adoc#_forbidden_imei_rules_cfg,forbidden_imei_rules.cfg>>:: конфигурация правил с перечислением масок запрещённых IMEI;
<<common/config/gtp_c.adoc#_gtp_c_cfg,gtp_c.cfg>>:: конфигурация настроек компонента GTP-C;
<<common/config/http.adoc#_http_cfg,http.cfg>>:: конфигурация настроек HTTP-соединений;
<<common/config/imsi_rules.adoc#_imsi_rules_cfg,imsi_rules.cfg>>:: конфигурация правил, связывающих TAC с регулярными выражениями для номеров IMSI;
<<common/config/long_mnc.adoc#_long_mnc_cfg,long_mnc.cfg>>:: конфигурация сетей PLMN, имеющих трехзначные коды MNC;
<<common/config/metrics.adoc#_metrics_cfg,metrics.cfg>>:: конфигурация настроек сбора метрик;
<<common/config/mme.adoc#_mme_cfg,mme.cfg>>:: конфигурация основных настроек PROTEI MME;
<<common/config/qos_rules.adoc#_qos_rules_cfg,qos_rules.cfg>>:: конфигурация правил, связывающих параметры QoS с регулярными выражениями IMSI и APN-NI, а также параметры VoPS с регулярными выражениями IMSI;
<<common/config/rac_rules.adoc#_rac_rules_cfg,rac_rules.cfg>>:: конфигурация правил, связывающих RAC и LAC с адресами узлов SGSN и сервером MSC;
<<common/config/s1ap.adoc#_s1ap_cfg,s1ap.cfg>>:: конфигурация настроек компонента S1AP;
<<common/config/served_plmn.adoc#_served_plmn_cfg,served_plmn.cfg>>:: конфигурация сетей, обслуживаемых узлом ММЕ;
<<common/config/sgd.adoc#_sgd_cfg,sgd.cfg>>:: конфигурация настроек интерфейса SGd;
<<common/config/sgsap.adoc#_sgsap_cfg,sgsap.cfg>>:: конфигурация настроек компонента SGsAP;
<<common/config/tac_rules.adoc#_tac_rules_cfg,tac_rules.cfg>>:: конфигурация правил, связывающих TAC и дополнительные параметры;
<<common/config/trace.adoc#_trace_cfg,trace.cfg>>:: конфигурация настроек подсистемы журналирования;
<<common/config/ue_trace.adoc#_ue_trace_cfg,ue_trace.cfg>>:: конфигурация отслеживаемых абонентов;
<<common/config/zc_rules.adoc#_zc_rules_cfg,zc_rules.cfg>>:: конфигурация правил, связывающих зоны и TAC;
<<common/config/component/_index.adoc#_config_files,component>>:: директория для конфигурационных файлов компонент:
[horizontal]
<<common/config/component/config.adoc#_config_cfg,component/config.cfg>>::: настройки компонент;
<<common/config/component/diameter.adoc#_diameter_component,component/diameter.cfg>>::: настройки компонента Diameter;
<<common/config/component/gtp_c.adoc#_gtp_c_component,component/gtp_c.cfg>>::: настройки компонента GTP-C;
<<common/config/component/s1ap.adoc#_s1ap_cfg_component,component/s1ap.cfg>>::: настройки компонента S1AP;
<<common/config/component/sgsap.adoc#_sgsap_cfg_component,component/sgsap.cfg>>::: настройки компонента SGsAP.

В описании каждого файла конфигурации приведены следующие сведения:

* Описание всех настраиваемых в файле параметров, включая тип данных, возможный диапазон значений, значение по умолчанию и единицы измерения (при наличии);
* Информация об обязательности задания значения каждого параметра — столбец O/M;
* Информация о необходимости перезапуска ПО узла SGW для применения нового значения параметра в случае изменения — столбец P/R;
* Информация о версиях приложения, в которых используется параметр — столбец "Версия" (отсутствие
информации означает, что параметр используется во всех версиях приложения);
* Работоспособный пример файла конфигурации.

== Условные обозначения в таблицах описания параметров конфигурации

Поле *O/M* — необходимость указания значения параметра:

[horizontal]
O (Optional):: опциональный параметр, может отсутствовать в конфигурации, компонентом используются значения по
умолчанию;
M (Mandatory):: обязательный параметр, при отсутствии в конфигурации не гарантируется работоспособность компонента;
C (Conditional):: требуется указание параметра при определенном условии (см. примечания в описании конфигурации).

Поле *P/R* — необходимость перезагрузки компонента для применения изменений параметра:

[horizontal]
P (Permanent):: изменения параметра не применяются динамически, требуется перезагрузка компонента;
R (Reloadable):: изменения параметра применяются динамически, перезагрузка компонента не требуется.

.Типы данных параметров конфигурации
[options="header",cols="1,6"]
|===
| Тип | Описание

| bool
| Логический тип. Задает флаг. Принимает два значения, `true` и `false`.

| flag
| Числовой тип. Задает флаг. Принимает два значения, `0` и `1`.

| string
| Строковый тип. Задает строку. Использует буквенные, цифровые и специальные символы.

| int
| Числовой тип. Задает целое 32—битное число, записанное цифрами 0—9 и знаком минуса "-". Диапазон: от -2^31^ до 2^31^ - 1.

| choice
| Коллекция. Задает объект с набором полей, для каждого экземпляра должно быть определено только одно любое поле.

| object
| Кортеж. Содержит фиксированное количество параметров различных типов.

| list
| Список. Содержит несколько значений одного типа или структуры.

| map
| Ассоциативный массив, словарь. Задает неупорядоченный набор пар ключ-значение.

| float
| Числовой тип. Задает число с плавающей точкой.

| double
| Числовой тип. Задает число с плавающей точкой двойной точности.

| datetime
| Тип для задания даты и времени. Формат по умолчанию: +
*YYYY-MM-DD hh:mm:ss.mss* +
YYYY — год; MM — месяц; DD — день; +
hh — час; mm — минута; ss — секунда; mss — миллисекунда. +
Время задается в формате 24-часового дня.

| hex
| Числовой тип. Задает целое число в формате шестнадцатеричного числа, записанного цифрами 0—9 и буквами A—F. Числу может предшествовать обозначение 0x. При отсутствии обозначения определяется как строка.

| ip
| Строковый тип. Задает IP-адрес версии 4: `xxx.xxx.xxx.xxx`

| None
| Нулевой тип. Не задает значение, необходима лишь инициализация объекта.

| units
| Объект. Задает величину и единицы измерения. +
`d` — день;/`h` — час;/`m` — минута;/`s` — секунда;/`ms` — миллисекунда;/`us` — микросекунда; +
`b` — биты;/`Kb` — килобиты;/`Mb` — мегабиты;/`Gb` — гигабиты;/`Tb` — терабиты; +
`B` — байты;/`KB` — килобайты;/`MB` — мегабайты;/`GB` — гигабайты;/`TB` — терабайты.
|===

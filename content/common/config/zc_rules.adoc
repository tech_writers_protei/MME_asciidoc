---
title: "zc_rules.cfg"
description: "Параметры Zone Code и TAC"
type: docs
weight: 20
---
= zc_rules.cfg
include::../../ascii_parameters.adoc[]

В файле задаются правила, связывающие зоны и {TAC}. Каждому правилу соответствует одна секция.
Имя секции является именем правила и может использоваться в качестве ссылки в параметре
<<common/config/served_plmn.adoc#zc-rules,served_plmn.cfg {two-colons} ZC_rules>>.

Ключ для перегрузки - *reload served_plmn.cfg*.

.Описание параметров
[cols="3,8,2,1,1,2",options="header"]
|===

| Параметр | Описание | Тип | O/M | P/R | Версия

| ZC
| Код зоны.
| int
| M
| R
| 1.36.0.0

| [[_tac_zc_rules]]TAC
| Код зоны отслеживания. См. NOTE: ниже. +
Пустая строка означает, что данный ZC применим к любому TAC.
| string
| O
| R
| 1.36.0.0
|===

NOTE: <<_tac_zc_rules,TAC>> определяет список значений или диапазонов в формате int или hex. Диапазон задаётся парой чисел, разделенных дефисом.

.Пример
[source,ini]
----
[Zone1]
ZC = 1;
TAC = "1,5,10-15";

[Zone2]
ZC = 2;
TAC = "0x14,28";
----
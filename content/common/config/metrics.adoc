---
title: "metrics.cfg"
description: "Параметры сбора метрик"
type: docs
weight: 20
---
= metrics.cfg
include::../../ascii_parameters.adoc[]

В файле задаются настройки сбора метрик.

.Используемые секции
[horizontal]
<<_general,[General]>>:: общие параметры сбора метрик
<<_s1Interface,[s1Interface]>>:: параметры сбора метрик S1
<<_s1Attach,[s1Attach]>>:: параметры сбора метрик S1, связанных с регистрацией абонента
<<_s1Detach,[s1Detach]>>:: параметры сбора метрик S1, связанных с дерегистрацией абонента
<<_s1BearerActivation,[_s1beareractivation]>>:: параметры сбора метрик S1, связанных с активацией bearer-служб
<<_s1BearerDeactivation,[s1BearerDeactivation]>>:: параметры сбора метрик S1, связанных с деактивацией bearer-служб
<<_s1BearerModification,[s1BearerModification]>>:: параметры сбора метрик S1, связанных с модификацией bearer-служб
<<_s1Security,[s1Security]>>:: параметры сбора метрик S1, связанных с аутентификацией абонента
<<_s1Service,[s1Service]>>:: параметры сбора метрик S1, связанных с процедурой Service Req
<<_handover,[Handover]>>:: параметры сбора метрик, связанных с процедурой Handover
<<_tau,[tau]>>:: параметры сбора метрик, связанных с процедурой Tracking Area Update
<<_sgsInterface,[sgsInterface]>>:: параметры сбора метрик SGs
<<_svInterface,[svInterface]>>:: параметры сбора метрик Sv (SRVCC HO)
<<_resource,[resource]>>:: параметры сбора метрик, связанных с ресурсами хоста
<<_users,[users]>>:: параметры сбора различных счётчиков
<<_s11Interface,[s11Interface]>>:: параметры сбора метрик S11
<<_paging,[paging]>>:: параметры сбора метрик, связанных с процедурой Paging

.Описание параметров
[cols="3,8,2,1,1,2",options="header"]
|===

| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[_general]][General]*
| Общие параметры сбора метрик.
|
| O
| P
| 1.45.0.0

| Enable
| Флаг сбора метрик. +
По умолчанию: 0.
| bool
| O
| P
| 1.45.0.0

| Directory
| Директория для хранения метрик. +
По умолчанию: ../metrics.
| string
| O
| P
| 1.45.0.0

| [[_granularity]]Granularity
| Гранулярность сбора метрик. +
По умолчанию: 5.
| int +
мин
| O
| P
| 1.45.0.0

| [[_node_name]]NodeName
| Имя узла, указываемое в названии файла с метриками.
| string
| O
| P
| 1.45.0.0

| *[[_s1Interface]][s1Interface]*
| Параметры для метрик S1.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0

| *[[_s1Attach]][s1Attach]*
| Параметры для метрик S1, связанных с регистрацией абонента.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0

| *[[_s1Detach]][s1Detach]*
| Параметры для метрик S1, связанных с дерегистрацией абонента.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_s1BearerActivation]][s1BearerActivation]*
| Параметры для метрик S1, связанных с активацией bearer-службы.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0

| *[[_s1BearerDeactivation]][s1BearerDeactivation]*
| Параметры для метрик S1, связанных с деактивацией bearer-службы.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0

| *[[_s1BearerModification]][s1BearerModification]*
| Параметры для метрик S1, связанных с модификацией bearer-службы.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_s1Security]][s1Security]*
| Параметры для метрик S1, связанных с аутентификацией абонента.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_s1Service]][s1Service]*
| Параметры для метрик S1, связанных с процедурой Service Request.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_handover]][handover]*
| Параметры для метрик, связанных с процедурой Handover.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_tau]][tau]*
| Параметры для метрик, связанных с процедурой Tracking Area Update.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_sgsInterface]][sgsInterface]*
| Параметры для метрик SGs.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_svInterface]][svInterface]*
| Параметры для метрик Sv (SRVCC HO).
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_resource]][resource]*
| Параметры для метрик, связанных с ресурсами хоста.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_users]][users]*
| Параметры сбора различных счётчиков.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_s11Interface]][s11Interface]*
| Параметры для метрик S11.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0

| *[[_paging]][paging]*
| Параметры для метрик, связанных с процедурой Paging.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.46.0.0

| *[[_s6a_interface]][S6a-interface]*
| Параметры для метрик S6a.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик. +
По умолчанию: значение <<_granularity,[General] {two-colons} Granularity>>.
| int +
мин
| O
| P
| 1.45.0.0
|===

.Пример
[source,ini]
----
[General]
Enable = 1;
Granularity = 30;
Directory = "/usr/protei/Protei_MME/metrics";
NodeName = MME-1;

[Diameter]
Enable = 1;
Granularity = 15;

[S6a-interface]
Enable = 1;
Granularity = 1;
----